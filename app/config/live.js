const props = require('./props.js');
let props_base = { ...props.props_base() };

props_base.app_properties.securityConf = {
  urlGrantingTicket: 'https://cotiza-i-pe.live-02.nextgen.igrupobbva/TechArchitecture/pe/grantingTicket/V02',
  urlAplication: 'https://cotiza-i-pe.live-03.platform.bbva.com/',
  aapId: '13000066',
  hostGetUser: 'https://cotiza-i-pe.live-02.nextgen.igrupobbva',
  pathGetUser: 'idm/v1/users?userId'
}; 

props_base.app_properties.services= {
  host: 'https://cotiza-i-pe.live-02.nextgen.igrupobbva',
  paths: {}
}; 

const appConfig = { ...props_base };

module.exports = appConfig;