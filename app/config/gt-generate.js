const props = require("./props.js");
let props_base = { ...props.props_base() };

props_base.app_properties.authenticationMode = 'generateGT';
props_base.app_properties.userIntegrated = false;

props_base.app_properties.securityConf = {
    urlGrantingTicket: 'https://aus-arqaso.work.pe.nextgen.igrupobbva:8050/TechArchitecture/pe/grantingTicket/V02',
    ivUser: 'pe.p121318',
    ivPassword: 'iTM1hMBjei5BDWn9biDVarysHyWAoU/81vHOMBa/Oew/8EfyhjoswQ==',
    aapId: '13000004'
};

props_base.app_properties.services = {
    host: 'https://aus-arqaso.work.pe.nextgen.igrupobbva:8050',
    paths: {}
};

const appConfig = { ...props_base };

module.exports = appConfig;
