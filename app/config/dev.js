
const props = require('./props.js');
let props_base = { ...props.props_base() };

props_base.app_properties.securityConf = {
  urlGrantingTicket: 'https://dev-arqaso.work.pe.nextgen.igrupobbva:8050/TechArchitecture/pe/grantingTicket/V02',
  urlAplication: 'https://ei-community.grupobbva.com/EAISAML/ksni_mult_web/',
  aapId: '13000066',
  hostGetUser: 'https://dev-arqaso.work.pe.nextgen.igrupobbva:8050',
  pathGetUser: 'idm/v1/users?userId'
}; 

props_base.app_properties.services= {
  host: 'https://dev-arqaso.work.pe.nextgen.igrupobbva:8050',
  paths: {}
}; 

const appConfig = { ...props_base };

module.exports = appConfig;