(function() {
  'use strict';
  window.CellsPolymer.start({
    routes: {
      'init': '/',
      'data-table': '/data-table',
      'combo-box': '/combo-box',
      'panel': '/panel-card',
      'text-editor': '/text-editor'
    }
  });
}());
