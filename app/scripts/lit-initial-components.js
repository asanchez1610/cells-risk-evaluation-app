// Import here your LitElement initial components (critical / startup)

import '@bbva-web-components/bbva-core-scoping-ambients-shim';
import '@webcomponents/shadycss/entrypoints/custom-style-interface.js';
import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import '@cells-components/coronita-icons';
import '@bbva-commons-web-components/cells-header-menu-app/cells-header-menu-app';
import '@commons-components/cells-commons-access-control-pe/cells-commons-access-control-pe';
import '@commons-components/cells-commons-dp-pe/cells-commons-dp-pe';
import '@bbva-web-components/bbva-help-modal/bbva-help-modal';