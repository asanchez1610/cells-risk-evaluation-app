document.addEventListener("DOMContentLoaded", async() => {
  let confs = window.AppConfig;
  let accessControl = document.querySelector("cells-commons-access-control-pe");
  accessControl.setAttribute(
    "url-granting-ticket",
    confs.securityConf.urlGrantingTicket
  );
  accessControl.setAttribute(
    "url-aplication",
    confs.securityConf.urlAplication
  );
  accessControl.setAttribute("aap-id", confs.securityConf.aapId);
  accessControl.setAttribute("host-get-user", confs.securityConf.hostGetUser);
  accessControl.setAttribute("path-get-user", confs.securityConf.pathGetUser);
  accessControl.setAttribute("authentication-mode", confs.authenticationMode);
  accessControl.userIntegrated = confs.userIntegrated;

  if(confs.authenticationMode === 'generateGT') {
    accessControl.setAttribute("user-id", confs.securityConf.ivUser);
    accessControl.setAttribute("iv-ticket", confs.securityConf.ivPassword);
  }

  let headerMenu = document.querySelector("cells-header-menu-app");
  headerMenu.imgFProfile = confs.imgFProfile;
  headerMenu.imgMProfile = confs.imgMProfile;
  headerMenu.titleApp = confs.titleApp;
  headerMenu.logo = confs.logo;
  headerMenu.logoMenu = confs.logoMenu;
  headerMenu.viewMode = confs.viewMode;
  const options = await fetch('resources/mock/options.json').then(response => response.json());
  document.querySelector('cells-header-menu-app').items = options;
  let user;
  if(confs.authenticationMode !== 'employee') {
    user = {"id":"5fc9400b3540d27ff226e289","email":"asanchez.sys@gmail.com","registro":"P027140","nombres":"Jose Arturo","apellidos":"Sanchez Soto","oficina":{"name":"Miraflores","code":"0333"},"sexo":"M","rol":{"name":"Administrador","code":"admin"},"token":""};
    headerMenu.user = user;
  }

  accessControl.addEventListener("app-autenthication-success", async({ detail }) => {
    console.log('app-autenthication-success', detail);
    if (confs.authenticationMode === 'employee') {
      user = {
          id: detail.data[0].id,
          email: detail.data[0].employee.email,
          registro: detail.data[0].employee.id,
          nombres: detail.data[0].firstName,
          apellidos: detail.data[0].fullLastName,
          oficina: { name: detail.data[0].budgetCenter.description, code: detail.data[0].budgetCenter.number },
          sexo: detail.data[0].employee.gender.substr(0, 1),
          rol: { name: "Administrador", code: "admin" }
        };
      headerMenu.user = user;
    }
  });

  window.addEventListener('error-request-dp-pe', ({detail})=> {
    console.log('error-request-dp-pe', detail);
    document.querySelector('#textMsg').innerHTML = `${detail.errorMessage ? detail.errorMessage : detail.systemErrorDescription ? detail.systemErrorDescription : 'Ha ocurrido un error inesperado.'}`;
    document.querySelector('#msgModal').open();
  });

  window.addEventListener('error-authentication', ({detail})=> {
    console.log('error-authentication', detail);
    document.querySelector('#textMsg').innerHTML = `${detail.message}`;
    document.querySelector('#msgModal').open();
  });

  window.addEventListener('on-generate-tsec', async({detail})=> {
    console.log('on-generate-tsec', detail);
    accessControl.setAttribute(
      "url-granting-ticket",
      detail.urlGrantingTicket
    );
    accessControl.setAttribute("user-id", detail.ivUser);
    accessControl.setAttribute("iv-ticket", detail.ivPassword);
    accessControl.setAttribute("aap-id", detail.aapId);
    await accessControl.runEmployeeAuthentication();
  });
  
});
