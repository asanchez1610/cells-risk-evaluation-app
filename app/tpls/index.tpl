<!doctype html>

<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Risk Evaluation App</title>
  <!-- Place favicon.ico in the `app/` directory -->

  <!-- Chrome for Android theme color -->
  <meta name="theme-color" content="#303F9F">

  <!-- Web Application Manifest -->
  <link rel="manifest" href="manifest.json">

  <!-- Tile color for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="staticScaffold">
  <link rel="icon" sizes="192x192" href="resources/images/touch/chrome-touch-icon-192x192.png">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="staticScaffold">
  <link rel="apple-touch-icon" href="resources/images/touch/apple-touch-icon.png">
  <!-- Tile icon for Win8 (144x144) -->
  <meta name="msapplication-TileImage" content="resources/images/touch/ms-touch-icon-144x144-precomposed.png">

  <link rel="stylesheet" href="styles/main.css">
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400&display=swap');
  </style>
  <script>
    window.IntlMsg = window.IntlMsg || {};
    window.IntlMsg.lang = 'es-ES';
  </script>
</head>

<body class="fullbleed layout vertical loading">
  <div id="splash">
 <div class="loadingio-spinner-ripple-8fpxb54ybkl">
          <div class="ldio-2hgp9rk3wnk">
              <div></div>
              <div></div>
          </div>
      </div>
  </div>
  <div id="app__content"></div>

 <cells-header-menu-app></cells-header-menu-app>

  <cells-commons-access-control-pe id="generic-access-control"></cells-commons-access-control-pe>

  <cells-commons-dp-pe id="cellsCommonsDp" genericAccessControl="#generic-access-control" ></cells-commons-dp-pe>

  <bbva-help-modal id="msgModal" button-text="Aceptar" title-icon="coronita:info" modal-type="warning">
          <div class="content" slot="slot-content">
            <div id="textMsg">
               
            </div>
          </div>
  </bbva-help-modal>

  <script src="resources/js/index.js" ></script>

  <!-- for a11y purposes -->
  <div id="announcer" aria-live="polite"></div>

  <script src="vendor/bowser.min.js"></script>
  <script type="module" src="scripts/app-module.js"></script>
  
</body>

</html>
