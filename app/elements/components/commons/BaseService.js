import { BaseElement } from "@bbva-commons-web-components/cells-base-elements/BaseElement";

export class BaseService extends BaseElement{
  constructor() {
    super();
    this.host = window.AppConfig.services.host;
    this.paths = window.AppConfig.services.paths;
  }

  generateRequest(settings) {
    settings.host = settings.host || this.host;
    const customEvent = new CustomEvent("send-request-dp-pe", {
      detail: settings,
      bubbles: true,
      composed: true,
    });
    window.dispatchEvent(customEvent);
  }
}
