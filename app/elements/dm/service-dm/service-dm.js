import { BaseService } from "../../components/commons/BaseService";

class ServiceDm extends BaseService {
  constructor() {
    super();
  }

  listTypeProducts() {
    return [
      {
        text: "PLD",
        value: "10",
      },
      {
        text: "Hipotecario",
        value: "11",
      },
      {
        text: "Comex",
        value: "12",
      },
      {
        text: "Préstamo comercial (LP)",
        value: "13",
      },
      {
        text: "Préstamo comercial (CP)",
        value: "14",
      },
    ];
  }

}
customElements.define("service-dm", ServiceDm);
