import { html, css } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import stylesPages from '../../elements/utils/styles-pages';
import '@bbva-commons-web-components/cells-rich-text-edit/cells-rich-text-edit';
import { MasterPage } from '../../elements/utils/master-page';
import '@bbva-commons-web-components/cells-card-panel/cells-card-panel';

class TextEditorPage extends MasterPage {
  static get is() {
    return 'text-editor-page';
  }

  constructor() {
    super();
  }

  static get properties() {
    return { };
  }

  render() {
    return this.content(html`
    <h2 class="title-page">Text Editor</h2>
    <p class="clone">
    Clone Component: <a href="https://gitlab.com/asanchez1610/cells-rich-text-edit" target="_blank" >https://gitlab.com/asanchez1610/cells-rich-text-edit</a>
    </p>

    <cells-card-panel class="blue" headerTitle="Cells Rich Text Edit">
      <div slot="body">
      <cells-rich-text-edit mode="full"></cells-rich-text-edit>
      </div>
    </cells-card-panel>

    `);
  }

  onPageEnter() {}

  onPageLeave() {}

  static get styles() {
    return [
      stylesPages,
      css`
        .blue {
              --header-bgcolor: #004481;
              --header-color: #fff;
              --header-btn-action-color-hover: #ddd7f7;
            }
    `
    ];
  }
}

window.customElements.define(TextEditorPage.is, TextEditorPage);
