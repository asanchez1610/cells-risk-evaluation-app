import { html, css } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import { MasterPage, gridStyles, stylesPages } from '../../elements/utils/master-page';
import '@bbva-web-components/bbva-form-field/bbva-form-field';
import '@bbva-web-components/bbva-button-default/bbva-button-default';
import '@bbva-commons-web-components/cells-combobox/cells-combobox';
import '@bbva-web-components/bbva-form-textarea/bbva-form-textarea';

class InitPage extends MasterPage {
  static get is() {
    return 'init-page';
  }

  static get properties() {
    return { methods: Array };
  }

  constructor() {
    super();
    this.methods = [
      {
        text: 'GET',
        value: 'GET'
      },
      {
        text: 'POST',
        value: 'POST'
      }
    ];
    console.log('this.appProps', this.appProps);
  }

  firstUpdated(changedProps) {
    this.element('cells-combobox').setSelected({
      text: 'GET',
      value: 'GET'
    }, 'GET');
  }

  generateTsec() {
    const inputs = this.elementsAll('.i-gt');
    console.log('generateTsec[inputs]', inputs);
    let data = {};
    inputs.forEach(input => {
      if (input.name) {
        data[input.name] = input.value;
      }
    });
    this.dispatch('on-generate-tsec', data);
  }

  testService() {
    const inputs = this.elementsAll('.i-test');
    console.log('testService[inputs]', inputs);
    this.element('#code-result').innerHTML = 'Procesando...';
    let data = {};
    inputs.forEach(input => {
      if (input.name && this.isNotBlank(input.value)) {
        data[input.name] = input.value.trim();
      }
    });
    data.onSuccess = (response)=> {
      console.log('onSuccess', response);
      this.element('#code-result').innerHTML = JSON.stringify(response.detail, null, 2);
    };
    data.onError = (error)=>{
      console.log('onError', error);
      this.element('#code-result').innerHTML = JSON.stringify(error, null, 2);
    };
    console.log('generateTsec[data]', data);
    console.log('testService[data]', data);
    this.dispatch('send-request-dp-pe', data);
  }

  get isAuthGenerateTsec() {
    return this.extract(this.appProps, 'authenticationMode', '') !== 'generateGT';
  }

  render() {
    return this.content(html`
        <h2 class="title-page">Generate Granting Ticket</h2>
         
        <div class="row">
          <div class="col-12	col-sm-12 col-md-12 col-lg-12 col-xl-12	col-xxl-12 col-section">
            <bbva-form-field class="i-gt" name="urlGrantingTicket" .value="${this.extract(this.appProps, 'securityConf.urlGrantingTicket', '')}" label="Url Granting Ticket" optional-label="" ></bbva-form-field>
          </div>
        </div>

        <div class="row">
          <div class="col-12	col-sm-12 col-md-12 col-lg-3 col-xl-3	col-xxl-3 col-section">
              <bbva-form-field .disabled="${this.isAuthGenerateTsec}" class="i-gt" name="ivUser" .value="${this.extract(this.appProps, 'securityConf.ivUser', '')}" label="Iv User" optional-label="" ></bbva-form-field>
          </div>
          <div class="col-12	col-sm-12 col-md-12 col-lg-5 col-xl-5	col-xxl-5 col-section">
            <bbva-form-field .disabled="${this.isAuthGenerateTsec}" class="i-gt" name="ivPassword" .value="${this.extract(this.appProps, 'securityConf.ivPassword', '')}" label="Iv Ticket" optional-label="" ></bbva-form-field>
          </div>
          <div class="col-12	col-sm-12 col-md-12 col-lg-2 col-xl-2	col-xxl-2 col-section">
            <bbva-form-field class="i-gt" name="aapId" .value="${this.extract(this.appProps, 'securityConf.aapId', '')}"  label="AAP" optional-label="" ></bbva-form-field>
          </div>
          <div class="col-12	col-sm-12 col-md-12 col-lg-2 col-xl-2	col-xxl-2 col-section">
            <bbva-button-default @click="${this.generateTsec}" class="btn-full-width" text="Generar GT" ></bbva-button-default>
          </div>
        </div>
        <br>
        <h2 class="title-page">Test Service with GT</h2>
        <div class="row">
          <div class="col-12	col-sm-12 col-md-5 col-lg-5 col-xl-5	col-xxl-5">
            <bbva-form-field name="host" class="col-section i-test" value="${this.extract(this.appProps, 'services.host')}" label="Host" optional-label="" ></bbva-form-field>
            <bbva-form-field name="path" class="col-section i-test" value="customers/v0/customers?customer.id=25270414" label="Path" optional-label="" ></bbva-form-field>
            
            <div class="row">
              
            <div class="col-12	col-sm-12 col-md-12 col-lg-6 col-xl-6	col-xxl-6">
                <cells-combobox
                  name="method"
                  label="Method"
                  class="i-test"
                  .items="${this.methods}"
                ></cells-combobox>
              </div>
              <div class="col-12	col-sm-12 col-md-12 col-lg-6 col-xl-6	col-xxl-6">
                <bbva-button-default class="btn-full-width" text="Test" @click="${this.testService}" ></bbva-button-default>
              </div>
              <div class="col-12	col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12" style="margin-top:10px;" >
                <bbva-form-textarea name="body" class="i-test" label="Body"></bbva-form-textarea>
              </div>
            </div>

          </div>

          <div class="col-12	col-sm-12 col-md-7 col-lg-7 col-xl-7	col-xxl-7">

          <pre class='code code-json'><label>Result</label><code id="code-result">{}</code></pre>
          </div>
        </div>

    `);
  }

  onPageEnter() {}

  onPageLeave() {}

  static get styles() {
    return [stylesPages, gridStyles, css`
      @media only screen and (max-width: 990px) {
        bbva-button-default {
          width: 100%;
          max-width: 100%;
        }
      }
    `];
  }
}

window.customElements.define(InitPage.is, InitPage);
