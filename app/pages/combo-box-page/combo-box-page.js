import { html, css } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import stylesPages from '../../elements/utils/styles-pages';
import '@bbva-commons-web-components/cells-combobox/cells-combobox';
import { MasterPage } from '../../elements/utils/master-page';
import '../../elements/dm/service-dm/service-dm';

class ComboBoxPage extends MasterPage {
  static get is() {
    return 'combo-box-page';
  }

  constructor() {
    super();
    this.products = [];
    this.initPage();
  }

  get service() {
    return this.element('service-dm');
  }

  async initPage() {
    await this.updateComplete;
    this.element('cells-combobox').selectMaskLoading = true;
    setTimeout(() => {
      this.element('cells-combobox').selectMaskLoading = false;
      this.products = this.service.listTypeProducts();
    }, 3000);
  }

  static get properties() {
    return { products: Array };
  }

  selectedProduct({detail}) {
    console.log('selectedProduct', detail);
    this.element('#code-result').innerHTML = JSON.stringify(detail, null, 2);
  }

  render() {
    return this.content(html`
     <h2 class="title-page">Combo Box</h2>
    <p class="clone">
      Clone Component: <a href="https://gitlab.com/asanchez1610/cells-combobox" target="_blank" >https://gitlab.com/asanchez1610/cells-combobox</a>
    </p>
    <cells-combobox
            label="Tipo de producto"
            .items="${this.products}"
            @option-selected="${this.selectedProduct}"
          ></cells-combobox>

          <pre class='code code-json'><label>Item</label><code id="code-result">{}</code></pre>
    <service-dm></service-dm>      
          `);
  }

  onPageEnter() {}

  onPageLeave() {}

  static get styles() {
    return [
      stylesPages,
      css` 
      cells-combobox{
        margin-bottom: 10px;
      }
    `
    ];
  }
}

window.customElements.define(ComboBoxPage.is, ComboBoxPage);
