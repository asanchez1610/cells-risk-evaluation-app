import { html, css } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import stylesPages from '../../elements/utils/styles-pages';
import '@bbva-commons-web-components/cells-card-panel/cells-card-panel';
import { MasterPage } from '../../elements/utils/master-page';

class PanelPage extends MasterPage {
  static get is() {
    return 'panel-page';
  }

  constructor() {
    super();
  }

  static get properties() {
    return { };
  }

  render() {
    return this.content(html`
    <h2 class="title-page">Panel Card</h2>
    <p class="clone">
    Clone Component: <a href="https://gitlab.com/asanchez1610/cells-card-panel" target="_blank" >https://gitlab.com/asanchez1610/cells-card-panel</a>
    </p>

            <cells-card-panel headerTitle="Card Panel component">
              <div slot="body">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </div>
            </cells-card-panel>

            <cells-card-panel class="blue" headerTitle="Card Panel component">
              <div slot="body">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </div>
            </cells-card-panel>

            <cells-card-panel class="green" headerTitle="Card Panel component">
              <div slot="body">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </div>
            </cells-card-panel>

            <cells-card-panel class="dark" headerTitle="Card Panel component">
              <div slot="body">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </div>
            </cells-card-panel>

            <cells-card-panel class="purple" headerTitle="Card Panel component">
              <div slot="body">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </div>
            </cells-card-panel>
          
          `);
  }

  onPageEnter() {}

  onPageLeave() {}

  static get styles() {
    return [
      stylesPages,
      css`
      cells-card-panel {
        margin-bottom: 20px;
      } 

      .blue {
              --header-bgcolor: #004481;
              --header-color: #fff;
              --header-btn-action-color-hover: #ddd7f7;
            }

      .dark {
        --header-bgcolor: #072146;
        --header-color: #fff;
        --header-btn-action-color-hover: #ddd7f7;
      }

      .green {
        --header-bgcolor: #277a3e;
        --header-color: #ffff;
        --header-btn-action-color-hover: #ddd7f7;
      }

      .purple {
        --header-bgcolor: #8f7ae5;
        --header-color: #fff;
        --header-btn-action-color-hover: #ddd7f7;
      }
    `
    ];
  }
}

window.customElements.define(PanelPage.is, PanelPage);
