import { html, css } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import stylesPages from '../../elements/utils/styles-pages';
import '@bbva-commons-web-components/cells-data-table/cells-data-table';
import { MasterPage } from '../../elements/utils/master-page';

class DataTablePage extends MasterPage {
  static get is() {
    return 'data-table-page';
  }

  async initDataTableMemory() {
    let columnsConfig = [
      {
        title: 'Código',
        dataField: 'customerId',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Nombre',
        dataField: 'first_name',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Apellido',
        dataField: 'last_name',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Email',
        dataField: 'email',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Sexo',
        dataField: 'gender',
        columnRender: (value, index, row) => {
          return value === 'Male' ? 'Masculino' : 'Femenino';
        },
      },
      {
        title: 'Nacionalidad',
        dataField: 'nationality',
        columnRender: (value, index, row) => {
          return value;
        },
      },
    ];

    const items = await fetch(
      'resources/mock/customers-100.json'
    ).then((response) => response.json());
    console.log(items);
    let dataTable = this.shadowRoot.querySelector('#dataTable');
    dataTable.columnsConfig = columnsConfig;
    dataTable.items = items;
  }

  constructor() {
    super();
    this.initPageDataTable();
  }

  async initPageDataTable() {
    await this.updateComplete;
    this.initDataTableMemory();
  }

  render() {
    return this.content(html`
          <h2 class="title-page">Data Table Example</h2>
          <p class="clone">
            Clone Component: <a href="https://gitlab.com/asanchez1610/cells-data-table" target="_blank" >https://gitlab.com/asanchez1610/cells-data-table</a>
          </p>
          <cells-data-table
          checkbox-selection
          pageSize="10"
          modePagination="memory"
          cls="navy"
          id="dataTable"
        ></cells-data-table>
    `);
  }

  onPageEnter() {}

  onPageLeave() {}

  static get styles() {
    return [ stylesPages ];
  }
}

window.customElements.define(DataTablePage.is, DataTablePage);
